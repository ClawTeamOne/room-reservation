import React from "react";
import { BrowserRouter, Route } from 'react-router-dom';
import Login from './pages/Login'

import Main from './pages/Main'

import ListRoom from './pages/ListRoom'
import RegisterRoom from './pages/RegisterRoom'
import UpdateRoom from './pages/UpdateRoom'

import ListReservations from './pages/ListReservations'
import RegisterReservations from './pages/RegisterReservations'
import UpdateReservations from './pages/UpdateReservations'

import ListUsers from './pages/ListUsers'

export default function Routes(){
    return (
        <BrowserRouter>
            <Route path="/" exact component={Login} />
            <Route path="/main" component={Main} />

            <Route path="/listroom" component={ListRoom} />
            <Route path="/registerroom" component={RegisterRoom} />
            <Route path="/updateroom/:id/:action" component={UpdateRoom} />

            <Route path="/listreservations" component={ListReservations} />
            <Route path="/registerreservations" component={RegisterReservations} />
            <Route path="/updatereservations/:id/:action" component={UpdateReservations} />

            <Route path="/listusers" component={ListUsers} />
        </BrowserRouter>
    );
}
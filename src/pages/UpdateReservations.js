import React, { useState, useEffect } from "react";
import logo from "../assets/logo-labtrans.png";
import { ToastsContainer, ToastsStore } from "react-toasts";
import { Link } from "react-router-dom";
import "./UpdateReservations.scss";
import api from "../services/api";
import md5 from "md5";

import DayPickerInput from "react-day-picker/DayPickerInput";
import "react-day-picker/lib/style.css";

export default function UpdateReservations({ history, match }) {
  const [coffee, setCoffee] = useState(false);
  const [people, setPeople] = useState(0);
  const [userSelected, setUserSelected] = useState("");
  const [responsible, setResponsible] = useState("");
  const [dateSelected, setDateSelected] = useState("");
  const [reservations, setReservations] = useState([]);
  const [actions, setActions] = useState("");
  const [name, setName] = useState("");
  const [roomSelected, setRoomSelected] = useState();

  useEffect(() => {
    (async function loadUsers() {
      if (!localStorage.getItem("access")) history.push(`/`);

      setUserSelected(localStorage.getItem("access"));

      setActions(match.params.action);
      setUserSelected(localStorage.getItem("access"));

      const response2 = await api.get(`/reservations/?id=${match.params.id}`);
      setReservations(response2.data[0]);

      setName(response2.data[0].name);
      setRoomSelected(response2.data[0].room_id);

      console.log(response2.data[0]);
    })();
  }, []);

  async function handleSubmit(e) {
    e.preventDefault();

    if (match.params.action === "edit") {
      const _date = new Date(dateSelected);
      const _day = _date.getDay() < 10 ? `0${_date.getDay()}` : _date.getDay();
      const _month =
        _date.getMonth() < 10 ? `0${_date.getMonth()}` : _date.getDay();
      const _year = _date.getFullYear();

      setDateSelected(`${_year}-${_month}-${_day}`);

      if (!coffee || !userSelected || !responsible || !dateSelected) {
        ToastsStore.error("Todos os campos são obrigatórios");
        return false;
      }

      const responsePut = await api.put(`/reservations/${match.params.id}`, {
        id: md5(new Date()),
        coffee,
        user_id: userSelected,
        room_id: roomSelected,
        responsible,
        date: dateSelected,
        people,
        name
      });

      console.log(responsePut.status);

      if (responsePut.status === 200)
        ToastsStore.success("Ação realizada com sucesso");
      else
        ToastsStore.error(
          "Ocorreu um erro ao realizar a ação, favor tente novamente"
        );
    } else if (match.params.action === "delete") {
      const responseDelete = await api.delete(
        `/reservations/${match.params.id}`
      );

      if (responseDelete.status === 200)
        ToastsStore.success("Ação realizada com sucesso");
      else
        ToastsStore.error(
          "Ocorreu um erro ao realizar a ação, favor tente novamente"
        );
    }

    setTimeout(() => {
      history.push(`/listreservations`);
    }, 4000);
  }

  return (
    <div className="reservations-container">
      <Link to="/main">
        <img className="app-logo" src={logo} alt="" />
      </Link>

      <form onSubmit={handleSubmit}>
        <label htmlFor="">Nome da Sala</label>
        <div className="name">{reservations.name}</div>

        <label htmlFor="">Tem Café?</label>
        <select onChange={e => setCoffee(e.target.value)}>
          <option selected value="">
            Selecione
          </option>
          <option value="true">Com</option>
          <option value="false">Sem</option>
        </select>
        {coffee === "true" ? (
          <div>
            <label htmlFor="">Escolha a quantidade de pessoas</label>
            <select
              value={reservations.people}
              onChange={e => setPeople(e.target.value)}
            >
              <option selected value="">
                Selecione
              </option>

              {Array.apply(0, Array(100)).map(function(x, i) {
                return (
                  <option key={i} value={i}>
                    {i} pessoas
                  </option>
                );
              })}
            </select>
          </div>
        ) : (
          ``
        )}

        <label htmlFor="">Escolha uma Data</label>
        <DayPickerInput
          placeholder="DD/MM/YYYY"
          format="DD/MM/YYYY"
          dayPickerProps={{
            modifiers: {
              disabled: [
                {
                  daysOfWeek: [0, 6]
                },
                {
                  before: new Date()
                }
              ]
            }
          }}
          onDayChange={e => setDateSelected(e)}
        />
        <label htmlFor=""></label>
        <input
          placeholder="Entre com o nome do responsável"
          onChange={e => setResponsible(e.target.value)}
          value={reservations.responsible}
        />
        <button type="submit">
          {actions === "edit" ? "EDITAR" : "EXCLUIR"}
        </button>
      </form>

      <button onClick={() => history.push("/listreservations")}>
        Voltar para a lista de resservas
      </button>
      <ToastsContainer store={ToastsStore} />
    </div>
  );
}

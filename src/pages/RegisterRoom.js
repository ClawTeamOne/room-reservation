import React, { useState, useEffect } from "react";
import logo from "../assets/logo-labtrans.png";
import { ToastsContainer, ToastsStore } from "react-toasts";
import { Link } from "react-router-dom";
import "./RegisterRoom.scss";
import api from "../services/api";
import md5 from "md5";

export default function RegisterRoom({ history, match }) {
  const [name, setName] = useState("");
  const [capacity, setCapacity] = useState("");

  useEffect(() => {
    (async function loadUsers() {
      if (!localStorage.getItem("access")) history.push(`/`);
    })();
  }, []);

  async function handleSubmit(e) {
    e.preventDefault();

    if (!name || !capacity) {
      ToastsStore.error("Todos os campos são obrigatórios");
      return false;
    }

    const response = await api.post(`/rooms`, {
      id: md5(new Date()),
      name: name,
      capacity: capacity
    });

    console.log(response);

    if (response.status === 201)
      ToastsStore.success("Sala cadastrada com sucesso");
    else
      ToastsStore.error(
        "Ocorreu um erro ao cadastrar a sala, favor tente novamente"
      );

    setTimeout(() => {
      history.push(`/listroom`);
    }, 4000);
  }

  return (
    <div className="room-container">
      <Link to="/main">
        <img className="app-logo" src={logo} alt="" />
      </Link>

      <form onSubmit={handleSubmit}>
        <input
          placeholder="Entre com o nome da sala"
          onChange={e => setName(e.target.value)}
        />
        <input
          placeholder="Entre com a capacidade da sala"
          type="number"
          onChange={e => setCapacity(e.target.value)}
        />
        <button type="submit">Cadastrar</button>
      </form>

      <button onClick={() => history.push("/listroom")}>
        Voltar para a lista de salas
      </button>
      <ToastsContainer store={ToastsStore} />
    </div>
  );
}

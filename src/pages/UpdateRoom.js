import React, { useState, useEffect } from "react";
import logo from "../assets/logo-labtrans.png";
import { ToastsContainer, ToastsStore } from "react-toasts";
import { Link } from "react-router-dom";
import "./ListRoom.scss";
import api from "../services/api";
import md5 from "md5";

export default function UpdateRoom({ history, match }) {
  const [name, setName] = useState("");
  const [capacity, setCapacity] = useState("");
  const [actions, setActions] = useState("");

  useEffect(() => {
    (async function loadRooms() {
      if (!localStorage.getItem("access")) history.push(`/`);

      const response = await api.get(`/rooms/${match.params.id}`);
      console.log(response.data);

      setName(response.data.name);
      setCapacity(response.data.capacity);
      setActions(match.params.action);
    })();
  }, [match.params.id]);

  async function handleSubmit(e) {
    e.preventDefault();

    if (match.params.action === "edit") {
      if (!name || !capacity) {
        ToastsStore.error("Todos os campos são obrigatórios");
        return false;
      }

      const responsePut = await api.put(`/rooms/${match.params.id}`, {
        name: name,
        capacity: capacity
      });
      console.log(responsePut.status);

      if (responsePut.status === 200)
        ToastsStore.success("Ação realizada com sucesso");
      else
        ToastsStore.error(
          "Ocorreu um erro ao realizar a ação, favor tente novamente"
        );
    } else if (match.params.action === "delete") {
      const responseDelete = await api.delete(`/rooms/${match.params.id}`);

      if (responseDelete.status === 200)
        ToastsStore.success("Ação realizada com sucesso");
      else
        ToastsStore.error(
          "Ocorreu um erro ao realizar a ação, favor tente novamente"
        );
    }

    setTimeout(() => {
      history.push(`/listroom`);
    }, 4000);
  }

  return (
    <div className="room-container">
      <Link to="/main">
        <img className="app-logo" src={logo} alt="" />
      </Link>

      <form onSubmit={handleSubmit}>
        <input
          placeholder="Entre com o nome da sala"
          value={name}
          onChange={e => setName(e.target.value)}
        />
        <input
          placeholder="Entre com a capacidade da sala"
          type="number"
          value={capacity}
          onChange={e => setCapacity(e.target.value)}
        />
        <button type="submit">
          {actions === "edit" ? "EDITAR" : "EXCLUIR"}
        </button>
      </form>

      <button onClick={() => history.push("/listroom")}>
        Voltar para a lista de salas
      </button>
      <ToastsContainer store={ToastsStore} />
    </div>
  );
}

import React, { useState, useEffect } from "react";
import logo from "../assets/logo-labtrans.png";
import { ToastsContainer, ToastsStore } from "react-toasts";
import { Link } from "react-router-dom";
import "./RegisterReservations.scss";
import api from "../services/api";
import md5 from "md5";

import DayPickerInput from "react-day-picker/DayPickerInput";
import "react-day-picker/lib/style.css";

export default function RegisterReservations({ history, match }) {
  const [coffee, setCoffee] = useState(false);
  const [people, setPeople] = useState(0);

  const [userSelected, setUserSelected] = useState("");

  const [rooms, setRooms] = useState([]);
  const [roomSelected, setRoomSelected] = useState();

  const [responsible, setResponsible] = useState("");

  const [dateSelected, setDateSelected] = useState("");

  useEffect(() => {
    (async function loadUsers() {
      if (!localStorage.getItem("access")) history.push(`/`);

      setUserSelected(localStorage.getItem("access"));

      const response = await api.get("/rooms");
      setRooms(response.data);

      console.log(response.data);
    })();
  }, []);

  async function handleSubmit(e) {
    e.preventDefault();

    const _date = new Date(dateSelected);
    const _day = _date.getDay() < 10 ? `0${_date.getDay()}` : _date.getDay();
    const _month =
      _date.getMonth() < 10 ? `0${_date.getMonth()}` : _date.getDay();
    const _year = _date.getFullYear();

    const newRoom = roomSelected.split(",");

    console.log(coffee);
    console.log(userSelected);
    console.log(roomSelected);
    console.log(responsible);
    console.log(`${_year}-${_month}-${_day}`);
    console.log(people);

    setDateSelected(`${_year}-${_month}-${_day}`);

    if (
      !coffee ||
      !userSelected ||
      !newRoom[0] ||
      !responsible ||
      !dateSelected
    ) {
      ToastsStore.error("Todos os campos são obrigatórios");
      return false;
    }

    console.log(newRoom[1]);

    const response = await api.post(`/reservations`, {
      id: md5(new Date()),
      coffee,
      user_id: userSelected,
      room_id: newRoom[0],
      responsible,
      date: dateSelected,
      people,
      name: newRoom[1]
    });

    console.log(response.data);

    if (response.status === 201)
      ToastsStore.success("Reserva cadastrada com sucesso");
    else
      ToastsStore.error(
        "Ocorreu um erro cadastrar sua reserva, favor tente novamente"
      );

    setTimeout(() => {
      history.push(`/listreservations`);
    }, 4000);
  }

  useEffect(() => {
    (async function loadUsers() {
      console.log(coffee);

      const qtd = rooms.map(value => {
        if (value.id === roomSelected) return value.capacity;
      });
    })();
  }, [coffee]);

  return (
    <div className="reservations-container">
      <Link to="/main">
        <img className="app-logo" src={logo} alt="" />
      </Link>

      <form onSubmit={handleSubmit}>
        <label htmlFor="">Escolha uma Sala</label>
        <select
          onChange={e => setRoomSelected(e.target.value)}
          value={roomSelected}
        >
          <option selected value="">
            Selecione
          </option>
          {rooms.map(value => (
            <option key={value.id} value={`${value.id}, ${value.name}`}>
              {value.name}
            </option>
          ))}
        </select>
        <label htmlFor="">Tem Café?</label>
        <select onChange={e => setCoffee(e.target.value)}>
          <option selected value="">
            Selecione
          </option>
          <option value="true">Com</option>
          <option value="false">Sem</option>
        </select>
        {coffee === "true" ? (
          <div>
            <label htmlFor="">Escolha a quantidade de pessoas</label>
            <select onChange={e => setPeople(e.target.value)}>
              <option selected value="">
                Selecione
              </option>

              {Array.apply(0, Array(100)).map(function(x, i) {
                return (
                  <option key={i} value={i}>
                    {i} pessoas
                  </option>
                );
              })}
            </select>
          </div>
        ) : (
          ``
        )}

        <label htmlFor="">Escolha uma Data</label>
        <DayPickerInput
          placeholder="DD/MM/YYYY"
          format="DD/MM/YYYY"
          dayPickerProps={{
            modifiers: {
              disabled: [
                {
                  daysOfWeek: [0, 6]
                },
                {
                  before: new Date()
                }
              ]
            }
          }}
          onDayChange={e => setDateSelected(e)}
        />
        <label htmlFor=""></label>
        <input
          placeholder="Entre com o nome do responsável"
          onChange={e => setResponsible(e.target.value)}
        />
        <button type="submit">Cadastrar</button>
      </form>

      <button onClick={() => history.push("/listreservations")}>
        Voltar para a lista de resservas
      </button>
      <ToastsContainer store={ToastsStore} />
    </div>
  );
}

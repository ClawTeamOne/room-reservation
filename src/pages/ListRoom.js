import React, { useState, useEffect } from "react";
import logo from "../assets/logo-labtrans.png";

import { Link } from "react-router-dom";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faEdit, faTimes } from "@fortawesome/free-solid-svg-icons";
import "./ListRoom.scss";
import api from "../services/api";

export default function Login({ history, match }) {
  const [rooms, setRooms] = useState("");

  useEffect(() => {
    (async function loadUsers() {
      if (!localStorage.getItem("access")) history.push(`/`);

      const response = await api.get("/rooms");
      setRooms(response.data);
    })();
  }, [match.params.id]);

  return (
    <div className="room-container">
      <Link to="/main">
        <img className="app-logo" src={logo} alt="" />
      </Link>
      {rooms.length > 0 ? (
        <ul>
          {rooms.map(room => (
            <li key={room.id}>
              <div
                className="title"
                title={`Nome: ${room.name}, Capacidade: ${room.capacity} pessoas`}
              >
                {room.name}
              </div>

              <div className="buttons">
                <button>
                  <FontAwesomeIcon
                    icon={faTimes}
                    onClick={() =>
                      history.push(`/updateroom/${room.id}/delete`)
                    }
                  />
                </button>
                <button>
                  <FontAwesomeIcon
                    icon={faEdit}
                    onClick={() => history.push(`/updateroom/${room.id}/edit`)}
                  />
                </button>
              </div>
            </li>
          ))}
        </ul>
      ) : (
        <div className="empty">Nenuma Sala encontrada</div>
      )}
      <button onClick={() => history.push("/registerroom")}>
        Cadastrar Nova Sala
      </button>
    </div>
  );
}

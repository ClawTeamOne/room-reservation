import React, { useEffect } from "react";
import logo from "../assets/logo-labtrans.png";
import "./Main.scss";

export default function Main({ history, match }) {
  useEffect(() => {
    (async function loadUsers() {
      if (!localStorage.getItem("access")) history.push(`/`);
    })();
  }, []);

  return (
    <div className="main-container">
      <img src={logo} className="App-logo" alt="logo" />

      <button onClick={() => history.push("/listroom")}>Lista de Salas</button>
      <button onClick={() => history.push("/listreservations")}>
        Lista de Reservas
      </button>
      <button onClick={() => history.push("/listusers")}>
        Lista de Usuários
      </button>
    </div>
  );
}

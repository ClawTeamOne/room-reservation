import React, { useState, useEffect } from "react";
import logo from "../assets/logo-labtrans.png";

import { Link } from "react-router-dom";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faEdit, faTimes } from "@fortawesome/free-solid-svg-icons";
import "./ListReservations.scss";
import api from "../services/api";

export default function Login({ history, match }) {
  const [reservations, setReservations] = useState("");

  useEffect(() => {
    (async function loadUsers() {
      if (!localStorage.getItem("access")) history.push(`/`);

      const response1 = await api.get("/reservations");

      setReservations(response1.data);
    })();
  }, [match.params.id]);

  return (
    <div className="reservations-container">
      <Link to="/main">
        <img className="app-logo" src={logo} alt="" />
      </Link>
      {reservations.length > 0 ? (
        <ul>
          {reservations.map(reservation => (
            <li key={reservation.id}>
              <div className="title" title={`Nome: ${reservation.name} `}>
                {reservation.name}
              </div>

              <div className="buttons">
                <button>
                  <FontAwesomeIcon
                    icon={faTimes}
                    onClick={() =>
                      history.push(
                        `/updatereservations/${reservation.id}/delete`
                      )
                    }
                  />
                </button>
                <button>
                  <FontAwesomeIcon
                    icon={faEdit}
                    onClick={() =>
                      history.push(`/updatereservations/${reservation.id}/edit`)
                    }
                  />
                </button>
              </div>
            </li>
          ))}
        </ul>
      ) : (
        <div className="empty">Nenuma reserva encontrada</div>
      )}
      <button onClick={() => history.push("/registerreservations")}>
        Fazer uma reserva
      </button>
    </div>
  );
}

import React, { useState } from "react";
import logo from "../assets/logo-labtrans.png";
import "./Login.scss";
import api from "../services/api";

export default function Login({ history }) {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");

  async function handleSubmit(e) {
    e.preventDefault();

    const response = await api.get(
      `/users/?login=${username}&password=${password}`
    );

    const { id } = response.data[0];

    if (response.data.length > 0){
      localStorage.setItem('access', id);
      history.push(`/main`);
    }
  }

  return (
    <div className="login-container">
      <form onSubmit={handleSubmit}>
        <img src={logo} className="App-logo" alt="logo" />
        <input
          placeholder="Entre com seu usuário"
          value={username}
          onChange={e => setUsername(e.target.value)}
        />
        <input
          placeholder="Entre com sua senha"
          value={password}
          onChange={e => setPassword(e.target.value)}
        />
        <button type="submit">Entrar</button>
      </form>
    </div>
  );
}

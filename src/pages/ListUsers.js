import React, { useState, useEffect } from "react";
import logo from "../assets/logo-labtrans.png";

import { Link } from "react-router-dom";
import "./ListUsers.scss";
import api from "../services/api";

export default function ListUser({ history, match }) {
  const [users, setUsers] = useState("");

  useEffect(() => {
    (async function loadUsers() {
      const response = await api.get("/users");
      setUsers(response.data);
    })();
  }, [match.params.id]);

  return (
    <div className="user-container">
      <Link to="/main">
        <img className="app-logo" src={logo} alt="" />
      </Link>
      {users.length > 0 ? (
        <ul>
          {users.map(user => (
            <li key={user.id}>
              <div
                className="title"
                title={`Nome: ${user.name}, Login: ${user.login}, Tipo: ${user.type}`}
              >
                {user.name}
              </div>
            </li>
          ))}
        </ul>
      ) : (
        <div className="empty">Nenum usuário encontrado</div>
      )}
    </div>
  );
}
